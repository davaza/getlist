import { IActionType } from '../common';
import { ActionTypes, AsyncActionTypes } from '../Actions/Consts';

/**
 * Состояние для Redux хранилища (стора).
 * @prop {boolean} loginStatus Состояние зарегистрированности пользователя.
 * @prop {boolean} loading Ожидание завершения процедуры авторизации (завершение логина).
 * @prop {boolean} counter Результат вычисления.
 * @prop {boolean} counterIsLoading Выполнение вычисления.
 */
export interface IStoreState {
    loginStatus: boolean;
    loading: boolean;
    counter: number;
    counterIsLoading: boolean;
    dataOrganizations: any;
    showStatusOrg: boolean;
    dataOrg: any;
    dataDepts: any;
    showStatusDepts: boolean;
    dataStaff: any;
    showStatusStaff: boolean;
    dataTest: string;
    editStatOrg: boolean;
    idParent: number;
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreState {
        return {
            loginStatus: false,
            loading: false,
            counter: 0,
            counterIsLoading: false,
            dataOrganizations: null,
            showStatusOrg: false,
            dataOrg: null,
            dataDepts: null,
            showStatusDepts: false,
            dataStaff: null,
            showStatusStaff: false,
            dataTest: null,
            editStatOrg: false,
            idParent: null,
        };
    }
};

export function reducer(state: IStoreState = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.CLICK}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                counterIsLoading: true,
            };

        case `${ActionTypes.CLICK}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                counterIsLoading: false,
                counter: state.counter + (action.payload || 1),
            };

        case `${AsyncActionTypes.BEGIN}`:
        case `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`:
        case `${ActionTypes.SHOW}${ActionTypes.ORGANIZATIONS}${AsyncActionTypes.BEGIN}`:
        case `${ActionTypes.SHOW}${ActionTypes.DEPTS}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                loading: true,
            };

        case `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                loginStatus: true,
                loading: false,
            };

        case `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                loading: false,
                loginStatus: false,
            };

        case ActionTypes.LOGOUT:
            return {
                ...state,
                loginStatus: false,
            };

        case `${ActionTypes.SHOW}${ActionTypes.ORGANIZATIONS}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                loading: false,
                dataOrganizations: action.payload,
                showStatusOrg: true,
            };
        case `${ActionTypes.SHOW}${ActionTypes.ORGANIZATIONS}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                loading: false,
                showStatusOrg: false,
            };
        case `${ActionTypes.SHOW}${ActionTypes.DEPTS}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                loading: false,
                dataDepts: action.payload,
                showStatusDepts: true,
            };
        case `${ActionTypes.SHOW}${ActionTypes.DEPTS}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                loading: false,
                showStatusDepts: false,
            };
        case `${ActionTypes.SHOW}${ActionTypes.STAFF}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                loading: false,
                dataStaff: action.payload,
                showStatusStaff: true,
            };
        case `${ActionTypes.SHOW}${ActionTypes.STAFF}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                loading: false,
                showStatusStaff: false,
            };
        case `GET_ID_PARENT`:
            return {
                ...state,
                idParent: action.payload,
            };
    }
    return state;
}

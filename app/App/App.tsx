import * as React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { IActionType } from '../common'
import { IOrganization, IDept, IStaff } from '../Actions/Models'
import { Form } from '../components/Form'
import { Organizations } from '../components/Organizations'
import { IStoreState } from '../Reducers/Reducers'
import { Actions } from '../Actions/Actions'
import './App.less'
import { Departments } from '../components/Departments';
import { Staff } from '../components/Staff';
import { BrowserRouter, Route } from 'react-router-dom';

/**
 * Пропсы компонента из стора.
 * @prop {boolean} loginStatus Состояние зарегистрированности пользователя.
 * @prop {boolean} loading Ожидание завершения процедуры авторизации (завершение логина) а так же загрузки данных.
 * @prop {boolean} countResult Результат вычисления.
 * @prop {boolean} counting Выполнение вычисления.
 * @prop {any} dataOrg Данные об организациях в формате json.
 * @prop {any} dataDepts Данные об отделах в формате json.
 * @prop {any} dataStaff Данные о сотрудниках в формате json.
 * @prop {boolean} showStatOrg Cостояние загрузки данных об организациях.
 * @prop {boolean} showStatDepts Cостояние загрузки данных об отделах.
 * @prop {boolean} showStatStaff Cостояние загрузки данных о сотрудниках.
 * @prop {number} idParent Id пункта из родительского списка. Нужен, например, когда переходим со списка организаций в список отделов при нажатии на один из пунктов организаций
 */

interface IStateProps {
  loginStatus: boolean;
  loading: boolean;
  countResult: number;
  counting: boolean;
  dataOrg: IOrganization[];
  showStatOrg: boolean;
  dataDepts: IDept[];
  showStatDepts: boolean;
  dataStaff: IStaff[];
  showStatStaff: boolean;
  idParent: number;
}

/**
 * Пропсы для передачи экшенов.
 * @prop {Actions} actions Экшены для работы приложения.
 */
export interface IDispatchProps {
  actions: Actions;
}

/**
 * Итоговые пропсы компонента
 */
type TProps = IStateProps & IDispatchProps

/**
 * Основной класс приложения.
 */
class App extends React.Component<TProps, {}> {
  /**
   * Обработчик запуска вычисления.
   */
  handleClick = () => this.props.actions.onClick(2)

  /**
   * Обработчик авторизации пользователя.
   */
  handleLogin = () =>
    this.props.actions.onLogin({ name: 'admin', password: '1' })

  /**
   * Обработчик выхода из системы.
   */
  handleLogout = () => this.props.actions.onLogout()

renderOrg = () => {
  const {showStatOrg, dataOrg, actions} = this.props;
  return (
    <Organizations showStatOrg={showStatOrg} dataOrg={dataOrg} actions={actions} />)
}

renderDept = () => {
  const {showStatDepts, dataDepts, actions, idParent} = this.props;
  return (
    <Departments showStatDepts={showStatDepts} dataDepts={dataDepts} actions={actions} idParent={idParent} />)
}
renderStaff = () => {
  const {showStatStaff, dataStaff, actions, idParent} = this.props;
  return (
    <Staff showStatStaff ={showStatStaff} actions={actions} dataStaff={dataStaff} idParent={idParent} />)
}

  render() {
    const { loginStatus, loading, countResult, counting, showStatOrg, showStatDepts, showStatStaff, actions } = this.props
    return (
      <BrowserRouter>
        <div className="container">
          {/* Форма авторизации. После того, как список с огранизациями загрузилась,
        ShowStatOrg = true и форма скрывается */}
          {showStatOrg || <Form actions={actions} />}
          {/* Если данные авторизации верные, т.е. loginStatus = true, то открывается страница организаций
          Аналогично для остальных страниц
          */}
          {loginStatus && <Route exact path="/organizations" render={this.renderOrg} />}
          {showStatDepts && <Route exact path="/departments" render={this.renderDept} />}
          {showStatStaff && <Route exact path="/staff" render={this.renderStaff} />}
          {/* Тут решил оставить старый пример Boilerplate.
          А так же присутствует вывод сообщения загрузки, входа в приложение */}
          <h3>Boilerplate</h3>
          {loading ? (
            <p>Загрузка...</p>
          ) : loginStatus ? (
            <p>Вы вошли как admin</p>
          ) : (
                <p>Вы еще не вошли в аккаунт!</p>
              )}
          <input
            className="btn btn-outline-secondary"
            disabled={loading}
            type="button"
            value="+"
            onClick={this.handleClick}
          />
          <input
            className="btn btn-outline-primary"
            disabled={loading}
            type="button"
            value="login"
            onClick={this.handleLogin}
          />
          <input
            className="btn btn-outline-warning"
            disabled={loading || counting}
            type="button"
            value="logout"
            onClick={this.handleLogout}
          />
          {counting && <p>Подсчет...</p>}
          {!counting && countResult > 0 && (
            <p className="red-color">{countResult}</p>
          )}
        </div>
      </BrowserRouter>
    )
  }
}

function mapStateToProps(state: IStoreState): IStateProps {
  return {
    loginStatus: state.loginStatus,
    loading: state.loading,
    countResult: state.counter,
    counting: state.counterIsLoading,
    dataOrg: state.dataOrganizations,
    showStatOrg: state.showStatusOrg,
    dataDepts: state.dataDepts,
    showStatDepts: state.showStatusDepts,
    dataStaff: state.dataStaff,
    showStatStaff: state.showStatusStaff,
    idParent: state.idParent,
  }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
  return {
    actions: new Actions(dispatch),
  }
}

const connectApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

export { connectApp as App }

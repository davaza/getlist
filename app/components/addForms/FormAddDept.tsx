import * as React from 'react'
import { Actions } from '../../Actions/Actions'
import { ActionTypes } from '../../Actions/Consts'

interface IStateAddDept {
  name_dept: string;
  tel_dept: string;
}

interface IPropsAdd {
  actions: Actions;
  idParent: number;
}

export class FormAddDept extends React.Component<IPropsAdd, IStateAddDept> {
  state: IStateAddDept = {
    name_dept: '',
    tel_dept: '',
  }
  onbtnClickHandler = (e: any) => {
    e.preventDefault()
    const { actions, idParent } = this.props
    const newItem = { id_ent: idParent, ...this.state }
    actions.addItemList(`${ActionTypes.DEPTS}`, newItem)
  }
  nameHandler = (e: any) => {
    this.setState({ name_dept: e.currentTarget.value })
  }
  telHandler = (e: any) => {
    this.setState({ tel_dept: e.currentTarget.value })
  }

  render() {
    const { name_dept, tel_dept } = this.state
    return (
      <React.Fragment>
        <div className="form-auth-wrap">
          <form className="form-auth clearfix">
            <h2>Добавить отдел</h2>
            <label className="clearfix">
              <span>Название:</span>
              <input
                type="text"
                id="name_dept"
                onChange={this.nameHandler}
                placeholder="Название отдела"
                value={name_dept}
              />
            </label>
            <label className="clearfix">
              <span>Телефон:</span>
              <input
                type="text"
                id="tel_dept"
                onChange={this.telHandler}
                placeholder="Телефон"
                value={tel_dept}
              />
            </label>
            <input
              type="submit"
              value="Добавить"
              onClick={this.onbtnClickHandler}
            />
          </form>
        </div>
      </React.Fragment>
    )
  }
}

import * as React from 'react'
import { Actions } from '../../Actions/Actions'
import { ActionTypes } from '../../Actions/Consts'

interface IStateFormAddOrg {
  name_entity: string;
  addr_entity: string;
  inn_entity: string;
}

interface IPropsAdd {
  actions: Actions;
}

export class FormAddOrg extends React.Component<IPropsAdd, IStateFormAddOrg> {
  state: IStateFormAddOrg = {
    name_entity: '',
    addr_entity: '',
    inn_entity: '',
  }
  onbtnClickHandler = (e: any) => {
    e.preventDefault()
    const { actions } = this.props
    actions.addItemList(`${ActionTypes.ORGANIZATIONS}`, this.state)
  }
  nameHandler = (e: any) => {
    this.setState({ name_entity: e.currentTarget.value })
  }
  addrHandler = (e: any) => {
    this.setState({ addr_entity: e.currentTarget.value })
  }
  innHandler = (e: any) => {
    this.setState({ inn_entity: e.currentTarget.value })
  }

  render() {
    const { name_entity, addr_entity, inn_entity } = this.state
    return (
      <React.Fragment>
        <div className="form-auth-wrap">
          <form className="form-auth clearfix">
            <h2>Добавить организацию</h2>
            <label className="clearfix">
              <span>Название:</span>
              <input
                type="text"
                id="name_entity"
                onChange={this.nameHandler}
                placeholder="Название организации"
                value={name_entity}
              />
            </label>
            <label className="clearfix">
              <span>Адрес:</span>
              <input
                type="text"
                id="addr"
                onChange={this.addrHandler}
                placeholder="Адрес"
                value={addr_entity}
              />
            </label>
            <label className="clearfix">
              <span>ИНН:</span>
              <input
                type="text"
                id="inn_entity"
                onChange={this.innHandler}
                placeholder="ИНН"
                value={inn_entity}
              />
            </label>
            <input
              type="submit"
              value="Добавить"
              onClick={this.onbtnClickHandler}
            />
          </form>
        </div>
      </React.Fragment>
    )
  }
}

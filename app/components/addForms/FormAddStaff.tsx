import * as React from 'react'
import { Actions } from '../../Actions/Actions'
import { ActionTypes } from '../../Actions/Consts'

interface IStateAddDept {
  fio_staff: string,
  addr_staff: string,
  job_staff: string
}

interface IPropsAdd {
  actions: Actions;
  idParent: number;
}

export class FormAddStaff extends React.Component<IPropsAdd, IStateAddDept> {
  state: IStateAddDept = {
    fio_staff: '',
    addr_staff: '',
    job_staff: ''
  }
  onbtnClickHandler = (e: any) => {
    e.preventDefault();
    const { actions, idParent } = this.props;
    const newItem = { id_dept_staff: idParent, ...this.state };
    actions.addItemList(`${ActionTypes.STAFF}`, newItem);
  }
  fioHandler = (e: any) => {
    this.setState({ fio_staff: e.currentTarget.value });
  }
  addrHandler = (e: any) => {
    this.setState({ addr_staff: e.currentTarget.value });
  }
  jobHandler = (e: any) => {
    this.setState({ job_staff: e.currentTarget.value });
  }

  render() {
    const { fio_staff, addr_staff, job_staff } = this.state;
    return (
      <React.Fragment>
        <div className="form-auth-wrap">
          <form className="form-auth clearfix">
            <h2>Добавить сотрудника</h2>
            <label className="clearfix">
              <span>Название:</span>
              <input
                type="text"
                id="fio_staff"
                onChange={this.fioHandler}
                placeholder="ФИО"
                value={fio_staff}
              />
            </label>
            <label className="clearfix">
              <span>Адрес:</span>
              <input
                type="text"
                id="addr_staff"
                onChange={this.addrHandler}
                placeholder="Адрес"
                value={addr_staff}
              />
            </label>
            <label className="clearfix">
              <span>Специальность:</span>
              <input
                type="text"
                id="job_staff"
                onChange={this.jobHandler}
                placeholder="Специальность"
                value={job_staff}
              />
            </label>
            <input type="submit" value="Добавить" onClick={this.onbtnClickHandler} />
          </form>
        </div>
      </React.Fragment>
    )
  }
}

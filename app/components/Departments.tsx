import * as React from 'react'
import { Actions } from '../Actions/Actions'
import { IDept } from '../Actions/Models'
import { Link } from 'react-router-dom'
import Modal from 'react-responsive-modal'
import { FormAddDept } from './addForms/FormAddDept'
import { DeptItem } from './items/DeptItem'

interface IStateDept {
  showForm: boolean;
}

interface IPropsDept {
  showStatDepts: boolean;
  dataDepts: IDept[];
  actions: Actions;
  idParent: number;
}

class Departments extends React.Component<IPropsDept, IStateDept> {
  state = {
    showForm: false,
  }

  addShowHandler = () => {
    this.setState({ showForm: true })
  }

  addHideHandler = () => {
    this.setState({ showForm: false })
  }
  renderComponent = () => {
    let data: any
    const { showStatDepts, dataDepts, actions } = this.props
    if (showStatDepts) {
      if (dataDepts.length) {
        data = dataDepts.map(function(item: any) {
          return (
            <DeptItem key={item.id} item={item} dataActions={actions} />
          )
        })
      } else {
        data = <p>К сожалению нет данных</p>
      }
    } else {
      return <p>Загрузка списка</p>
    }
    return data
  }
  render() {
    const { actions, idParent } = this.props
    const { showForm } = this.state
    return (
      <React.Fragment>
        <Modal open={showForm} onClose={this.addHideHandler}>
          <FormAddDept actions={actions} idParent={idParent} />
        </Modal>
        <h1>Список отделов</h1>
        <button
          type="button"
          className="btn btn-custom btn btn-outline-primary"
          onClick={this.addShowHandler}
        >
          Добавить отдел
        </button>
        <Link to="/organizations">
          <button type="button" className="btn btn-custom btn-outline-primary">
            Назад в список организаций
          </button>
        </Link>
        {this.renderComponent()}
      </React.Fragment>
    )
  }
}

export { Departments }

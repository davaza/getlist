import * as React from 'react';
import { Actions } from '../Actions/Actions'
import { Link } from 'react-router-dom';

interface IStateLogin {
  login: string;
  pass: string;
}

interface IPropsLogin{
  actions: Actions;
}

export class Form extends React.Component<IPropsLogin, IStateLogin> {
  /**
   *Обработчик проверки логина и пароля с формы
   */
  state: IStateLogin = {
    login: '',
    pass: '',
  }

  onbtnClickHandler = () => {
    const { login, pass } = this.state;
    this.props.actions.validation({ login, pass })
  }

  handleChangeLogin = (e: any) => {
    const { value } = e.currentTarget
    this.setState({ login: value })
  }

  handleChangePass = (e: any) => {
    const { value } = e.currentTarget
    this.setState({ pass: value })
  }

  render() {
    const { login, pass } = this.state
    return (
      <div className="form-auth-wrap">
        <form className="form-auth clearfix">
          <h2>Вход</h2>
          <label className="clearfix">
            Логин:
              <input
              type="text"
              id="login"
              onChange={this.handleChangeLogin}
              placeholder="Ваш логин"
              value={login}
            />
          </label>
          <label className="clearfix">
            Пароль:
              <input
              type="password"
              id="pass"
              onChange={this.handleChangePass}
              placeholder="Ваш пароль"
              value={pass}
            />
          </label>
          <Link to="/organizations"><button onClick={this.onbtnClickHandler}>Войти</button></Link>
        </form>
      </div>
    )
  }
}

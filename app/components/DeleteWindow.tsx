import * as React from 'react'
import { Actions } from '../Actions/Actions'

interface IPropsDelete {
  idElemForDel: number;
  dataActions: Actions;
  listType: string;
  idParent?: number;
  deleteHideHandler: Function;
}

export class DeleteWindow extends React.Component<IPropsDelete> {
  onBtnHide = () => {
    this.props.deleteHideHandler()
  }

  onBtnDelete = () => {
    const { idElemForDel, dataActions, listType, idParent } = this.props
    dataActions.deleteItemList(listType, idElemForDel, idParent)
  }

  render() {
    return (
      <React.Fragment>
        <div className="delete-wrap">
          <div className="question">Действительно хотите удалить?</div>
          <div className="answer-wrap">
            <button onClick={this.onBtnDelete}>Да</button>
            <button onClick={this.onBtnHide}>Нет</button>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

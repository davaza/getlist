import * as React from 'react'
import { Actions } from '../Actions/Actions'
import { IStaff } from '../Actions/Models'
import { Link } from 'react-router-dom'
import Modal from 'react-responsive-modal'
import { FormAddStaff } from './addForms/FormAddStaff'
import { StaffItem } from './items/StaffItem'

interface IStateStaff {
  showForm: boolean;
}

interface IPropsStaff {
  showStatStaff: boolean;
  actions: Actions;
  dataStaff: IStaff[];
  idParent: number;
}

class Staff extends React.Component<IPropsStaff, IStateStaff> {
  state: IStateStaff = {
    showForm: false,
  }

  addShowHandler = () => {
    this.setState({ showForm: true })
  }

  addHideHandler = () => {
    this.setState({ showForm: false })
  }

  renderComponent = () => {
    let data: any
    const { showStatStaff, actions, dataStaff } = this.props
    if (showStatStaff) {
      if (dataStaff.length) {
        data = dataStaff.map(function(item: any) {
          return (
            <StaffItem key={item.id} item={item} dataActions={actions} />
          )
        })
      } else {
        data = <p>К сожалению нет данных</p>
      }
    } else {
      return <p>Загрузка списка</p>
    }
    return data
  }
  render() {
    const { actions, idParent } = this.props
    const { showForm } = this.state
    return (
      <React.Fragment>
        <Modal open={showForm} onClose={this.addHideHandler}>
          <FormAddStaff actions={actions} idParent={idParent} />
        </Modal>
        <h1>Список сотрудников</h1>
        <button
          type="button"
          className="btn btn-custom btn btn-outline-primary"
          onClick={this.addShowHandler}
        >
          Добавить сотрудника
        </button>
        <Link to="/departments">
          <button type="button" className="btn btn-custom btn-outline-primary">
            Назад в список отделов
          </button>
        </Link>
        {this.renderComponent()}
      </React.Fragment>
    )
  }
}

export { Staff }

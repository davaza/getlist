import * as React from 'react';
import { ActionTypes, UrlTypes } from '../../Actions/Consts';
import { Actions } from '../../Actions/Actions'
import { IOrganization } from '../../Actions/Models'
import { Link } from 'react-router-dom';
import { FormEditOrg } from '../editForms/FormEditOrg';
import { DeleteWindow } from '../DeleteWindow';
import Modal from 'react-responsive-modal';

interface IStateItem {
  showForm: boolean;
  showDelMsg: boolean;
}

interface IPropsItem {
  dataActions: Actions;
  item: IOrganization;
}

export class OrgItem extends React.Component<IPropsItem, IStateItem>{
  state: IStateItem = {
    showForm: false,
    showDelMsg: false
  }
  onClickHandler = () => {
    const { id } = this.props.item;
    const { dataActions } = this.props;
    //При клике обращаемся к экшену для вывода списка отделов
    dataActions.showComponent(`${UrlTypes.URL_DEPT}`, `${ActionTypes.DEPTS}`, id);
  }
  editShowHandler = () => {
    this.setState({ showForm: true })
  }
  editHideHandler = () => {
    this.setState({ showForm: false })
  }

  deleteShowHandler = () => {
    this.setState({ showDelMsg: true })
  }

  deleteHideHandler = () => {
    this.setState({ showDelMsg: false })
  }

  render() {
    const { name_entity, addr_entity, inn_entity, id } = this.props.item;
    const { showForm, showDelMsg } = this.state;
    const { dataActions, item } = this.props;
    return (
      <React.Fragment>
        <Modal open={showForm} onClose={this.editHideHandler}>
          <FormEditOrg key={id} item={item} dataActions={dataActions} />
        </Modal>
        <Modal open={showDelMsg} onClose={this.deleteHideHandler}>
          <DeleteWindow deleteHideHandler={this.deleteHideHandler} idElemForDel={id} dataActions={dataActions} listType={ActionTypes.ORGANIZATIONS} />
        </Modal>
        <div className="item-org-wrap clearfix">
          <div className="clearfix"><span onClick={this.deleteShowHandler}>Удалить</span><span onClick={this.editShowHandler}>Редактировать</span></div>
          <ul className="item-org">
            <li>Название: {name_entity}</li>
            <li>Адрес: {addr_entity}</li>
            <li>ИНН: {inn_entity}</li>
          </ul>
          <span><Link to="/departments" onClick={this.onClickHandler}>Посмотреть список отделов</Link></span>
        </div>
      </React.Fragment>
    )
  }
}

import * as React from 'react'
import { ActionTypes } from '../../Actions/Consts'
import { Actions } from '../../Actions/Actions'
import { IStaff } from '../../Actions/Models'
import { FormEditStaff } from '../editForms/FormEditStaff'
import { DeleteWindow } from '../DeleteWindow'
import Modal from 'react-responsive-modal'

interface IStateItem {
  showForm: boolean;
  showDelMsg: boolean;
}

interface IPropsItem {
  dataActions: Actions;
  item: IStaff;
}

export class StaffItem extends React.Component<IPropsItem, IStateItem> {
  state: IStateItem = {
    showForm: false,
    showDelMsg: false
  }
  editShowHandler = () => {
    this.setState({ showForm: true })
  }

  editHideHandler = () => {
    this.setState({ showForm: false })
  }

  deleteShowHandler = () => {
    this.setState({ showDelMsg: true })
  }

  deleteHideHandler = () => {
    this.setState({ showDelMsg: false })
  }
  render() {
    const {
      fio_staff,
      addr_staff,
      job_staff,
      id,
      id_dept_staff,
    } = this.props.item
    const { showForm, showDelMsg } = this.state
    const { dataActions,item } = this.props
    return (
      <React.Fragment>
        <Modal open={showForm} onClose={this.editHideHandler}>
          <FormEditStaff key={id} dataActions={dataActions} item={item} />
        </Modal>
        <Modal open={showDelMsg} onClose={this.deleteHideHandler}>
          <DeleteWindow
            deleteHideHandler={this.deleteHideHandler}
            idElemForDel={id}
            idParent={id_dept_staff}
            dataActions={dataActions}
            listType={ActionTypes.STAFF}
          />
        </Modal>
        <div className="item-org-wrap clearfix">
          <div className="clearfix">
            <span onClick={this.deleteShowHandler}>Удалить</span>
            <span onClick={this.editShowHandler}>Редактировать</span>
          </div>
          <ul className="item-org">
            <li>ФИО: {fio_staff}</li>
            <li>Адрес: {addr_staff}</li>
            <li>Специальность: {job_staff}</li>
          </ul>
        </div>
      </React.Fragment>
    )
  }
}

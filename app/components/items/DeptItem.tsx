import * as React from 'react';
import { ActionTypes, UrlTypes } from '../../Actions/Consts';
import { Actions } from '../../Actions/Actions'
import { IDept } from '../../Actions/Models'
import { Link } from 'react-router-dom';
import { FormEditDept } from '../editForms/FormEditDept';
import { DeleteWindow } from '../DeleteWindow';
import Modal from 'react-responsive-modal';

interface IStateItem {
  showForm: boolean;
  showDelMsg: boolean;
}

interface IPropsItem {
  dataActions: Actions;
  item: IDept;
}

export class DeptItem extends React.Component<IPropsItem, IStateItem>{
  state: IStateItem = {
    showForm: false,
    showDelMsg: false,
  }

  editShowHandler = () => {
    this.setState({ showForm: true })
  }

  editHideHandler = () => {
    this.setState({ showForm: false })
  }

  onClickHandler = () => {
    const { dataActions, item } = this.props;

    dataActions.showComponent(`${UrlTypes.URL_STAFF}`, `${ActionTypes.STAFF}`, item.id)

  }

  deleteShowHandler = () => {
    this.setState({ showDelMsg: true })
  }

  deleteHideHandler = () => {
    this.setState({ showDelMsg: false })
  }
  render() {
    const { name_dept, tel_dept, id, id_ent } = this.props.item;
    const { showForm, showDelMsg } = this.state;
    const { dataActions,item } = this.props;
    return (
      <React.Fragment>
        <Modal open={showForm} onClose={this.editHideHandler}>
          <FormEditDept key={id} dataActions={dataActions} item={item} />
        </Modal>
        <Modal open={showDelMsg} onClose={this.deleteHideHandler}>
          <DeleteWindow deleteHideHandler={this.deleteHideHandler} idElemForDel={id} idParent={id_ent} dataActions={dataActions} listType={ActionTypes.DEPTS} />
        </Modal>
        <div className="item-org-wrap clearfix">
          <div className="clearfix"><span onClick={this.deleteShowHandler}>Удалить</span><span onClick={this.editShowHandler}>Редактировать</span></div>
          <ul className="item-org clearfix">
            <li>Название: {name_dept}</li>
            <li>Тел.: {tel_dept}</li>
            <span><Link to="/staff" onClick={this.onClickHandler}>Посмотреть список сотрудников</Link></span>
          </ul>
        </div>
      </React.Fragment>
    )
  }
}

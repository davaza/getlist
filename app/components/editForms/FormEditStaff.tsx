import * as React from 'react';
import { ActionTypes } from '../../Actions/Consts';
import { Actions } from '../../Actions/Actions'
import { IStaff } from '../../Actions/Models'

interface IStateEditStaff {
  fio_staff: string,
  addr_staff: string,
  job_staff: string,
}

interface IPropsFormEdit {
  dataActions: Actions;
  item: IStaff;
}

export class FormEditStaff extends React.Component<IPropsFormEdit, IStateEditStaff> {
  state: IStateEditStaff = {
    fio_staff: '',
    addr_staff: '',
    job_staff: ''
  }
  onbtnClickHandler = (e: any) => {
    e.preventDefault();
    const { dataActions } = this.props;
    const { id, id_dept_staff } = this.props.item;
    let newItem = { id, id_dept_staff, ...this.state }
    dataActions.editList(ActionTypes.STAFF, newItem);
  }
  componentDidMount() {
    const { fio_staff, addr_staff, job_staff } = this.props.item;
    this.setState({
      fio_staff: fio_staff,
      addr_staff: addr_staff,
      job_staff: job_staff,
    })
  }
  fioHandler = (e: any) => {
    this.setState({ fio_staff: e.currentTarget.value });
  }
  addrHandler = (e: any) => {
    this.setState({ addr_staff: e.currentTarget.value });
  }
  jobHandler = (e: any) => {
    this.setState({ job_staff: e.currentTarget.value });
  }

  render() {
    const { fio_staff, addr_staff, job_staff } = this.state;
    return (
      <React.Fragment>
        <div className="form-auth-wrap">
          <form className="form-auth clearfix">
            <h2>Изменить данные сотрудника</h2>
            <label className="clearfix">
              <span>ФИО:</span>
              <input
                type="text"
                id="fio_staff"
                onChange={this.fioHandler}
                placeholder="ФИО"
                value={fio_staff}
              />
            </label>
            <label className="clearfix">
              <span>Адрес:</span>
              <input
                type="text"
                id="addr_staff"
                onChange={this.addrHandler}
                placeholder="Адрес"
                value={addr_staff}
              />
            </label>
            <label className="clearfix">
              <span>Специальность:</span>
              <input
                type="text"
                id="job_staff"
                onChange={this.jobHandler}
                placeholder="Специальность"
                value={job_staff}
              />
            </label>
            <input type="submit" value="Сохранить" onClick={this.onbtnClickHandler} />
          </form>
        </div>
      </React.Fragment>
    )
  }
}

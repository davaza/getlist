import * as React from 'react';
import { ActionTypes } from '../../Actions/Consts';
import { Actions } from '../../Actions/Actions'
import { IDept } from '../../Actions/Models'

interface IStateEditDept {
  name_dept: string,
  tel_dept: string,
}

interface IPropsFormEdit{
  dataActions: Actions;
  item: IDept;
}

export class FormEditDept extends React.Component<IPropsFormEdit, IStateEditDept> {
  state: IStateEditDept = {
    name_dept: '',
    tel_dept: '',
  }
  onbtnClickHandler = (e: any) => {
    e.preventDefault();
    const { dataActions } = this.props;
    const { id, id_ent } = this.props.item;
    let newItem = { id, id_ent, ...this.state }
    dataActions.editList(ActionTypes.DEPTS, newItem);
  }
  componentDidMount() {
    const { name_dept, tel_dept } = this.props.item;
    this.setState({
      name_dept: name_dept,
      tel_dept: tel_dept
    })
  }
  nameHandler = (e: any) => {
    this.setState({ name_dept: e.currentTarget.value })
  }
  telHandler = (e: any) => {
    this.setState({ tel_dept: e.currentTarget.value })
  }

  render() {
    const { name_dept, tel_dept } = this.state
    return (
      <React.Fragment>
        <div className="form-auth-wrap">
          <form className="form-auth clearfix">
            <h2>Добавить отдел</h2>
            <label className="clearfix">
              <span>Название:</span>
              <input
                type="text"
                id="name_dept"
                onChange={this.nameHandler}
                placeholder="Название организации"
                value={name_dept}
              />
            </label>
            <label className="clearfix">
              <span>Телефон:</span>
              <input
                type="text"
                id="tel_dept"
                onChange={this.telHandler}
                placeholder="Телефон"
                value={tel_dept}
              />
            </label>
            <input
              type="submit"
              value="Сохранить"
              onClick={this.onbtnClickHandler}
            />
          </form>
        </div>
      </React.Fragment>
    )
  }
}

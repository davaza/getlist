import * as React from 'react'
import { IOrganization } from '../Actions/Models'
import { Actions } from '../Actions/Actions'
import { ActionTypes, UrlTypes } from '../Actions/Consts'
import Modal from 'react-responsive-modal';
import { OrgItem } from './items/OrgItem'
import { FormAddOrg } from './addForms/FormAddOrg'

interface IPropsOrg {
  showStatOrg: boolean;
  dataOrg: IOrganization[];
  actions: Actions;
}

interface IStateOrg {
  showForm: boolean;
}

class Organizations extends React.Component<IPropsOrg, IStateOrg> {
  state: IStateOrg = {
    showForm: false,
  }

  componentDidMount() {
    this.props.actions.showComponent(
      `${UrlTypes.URL_ORGANIZATION}`,
      `${ActionTypes.ORGANIZATIONS}`
    )
  }

  addShowHandler = () => {
    this.setState({ showForm: true })
  }

  addHideHandler = () => {
    this.setState({ showForm: false })
  }

  renderComponent = () => {
    let data: any;
    const { showStatOrg, dataOrg, actions} = this.props
    if (showStatOrg) {
      if (dataOrg.length) {
        data = dataOrg.map(function (item: any) {
          return <OrgItem key={item.id} item={item} dataActions={actions} />
        }
        )
      } else {
        data = <p>К сожалению нет данных</p>
      }
    } else {
      return <p>Загрузка списка</p>
    }
    return data;
  }
  render() {
    const { actions } = this.props
    const { showForm } = this.state;
    return (
      <div>
        <Modal open={showForm} onClose={this.addHideHandler}>
          <FormAddOrg actions={actions} />
        </Modal>
        <h1>Список организаций</h1>
        <button type="button" className="btn btn-custom btn-outline-primary" onClick={this.addShowHandler}>
          Добавить организацию
        </button>
        {this.renderComponent()}
      </div>
    )
  }
}

export { Organizations }

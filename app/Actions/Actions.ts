import { Dispatch } from 'redux';
import { IActionType } from '../common';
import { ActionTypes, AsyncActionTypes, UrlTypes } from './Consts';
import { ILoginData } from './Models';
import { IOrganization, IDept, IStaff } from '../Actions/Models'

interface ILoginValidation {
    login: string;
    pass: string;
}

/**
 * Экшены для приложения.
 */
export class Actions {
    constructor(private dispatch: Dispatch<IActionType>) {
    }

    deleteItemList = (listType: string, id_item: number, idParent: number) => {
        switch (listType) {
            case `${ActionTypes.ORGANIZATIONS}`:
                {
                    const options = {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'DELETE',
                    };

                    fetch(`${UrlTypes.URL_ORGANIZATION}/${id_item}`, options)
                        .then(response => {
                            if (response.status === 200) {
                                this.showComponent(
                                    `${UrlTypes.URL_ORGANIZATION}`,
                                    `${ActionTypes.ORGANIZATIONS}`
                                )
                                return Promise.resolve(response)
                            } else {
                                throw 'Не статус 200 ';
                            }
                        })
                        .catch(error => {
                            console.error('Ошибка: ', error);

                        });
                }
                break;

            case `${ActionTypes.DEPTS}`:
                {
                    const options = {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'DELETE',
                    };

                    fetch(`${UrlTypes.URL_DEPT}/${id_item}`, options)
                        .then(response => {
                            if (response.status === 200) {
                                this.showComponent(
                                    `${UrlTypes.URL_DEPT}`,
                                    `${ActionTypes.DEPTS}`,
                                    idParent
                                )
                                return Promise.resolve(response)
                            } else {
                                throw 'Не статус 200 ';
                            }
                        })
                        .catch(error => {
                            console.error('Ошибка: ', error);
                        });
                }
                break;

            case `${ActionTypes.STAFF}`:
                {
                    const options = {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'DELETE'
                    };

                    fetch(`${UrlTypes.URL_STAFF}/${id_item}`, options)
                        .then(response => {
                            if (response.status === 200) {
                                this.showComponent(
                                    `${UrlTypes.URL_STAFF}`,
                                    `${ActionTypes.STAFF}`,
                                    idParent
                                )
                                return Promise.resolve(response)
                            } else {
                                throw 'Не статус 200 ';
                            }
                        })
                        .catch(error => {
                            console.error('Ошибка: ', error);
                        });
                }
                break;
        }
    }

    //Добавление пунктов в списки
    addItemList = (listType: string, item: any) => {
        switch (listType) {
            case `${ActionTypes.ORGANIZATIONS}`:
                {
                    const options = {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'POST',
                        body: JSON.stringify(item),
                    };

                    fetch(`${UrlTypes.URL_ORGANIZATION}`, options)
                        .then(response => {
                            if (response.status === 201) {
                                this.showComponent(
                                    `${UrlTypes.URL_ORGANIZATION}`,
                                    `${ActionTypes.ORGANIZATIONS}`
                                )
                                return Promise.resolve(response)
                            } else {
                                throw 'Не статус 201 ';
                            }
                        })
                        .catch(error => {
                            console.error('Ошибка: ', error);
                        });
                }
                break;

            case `${ActionTypes.DEPTS}`:
                {
                    const { id_ent } = item;
                    const options = {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'POST',
                        body: JSON.stringify(item),
                    };

                    fetch(`${UrlTypes.URL_DEPT}`, options)
                        .then(response => {
                            if (response.status === 201) {
                                this.showComponent(
                                    `${UrlTypes.URL_DEPT}`,
                                    `${ActionTypes.DEPTS}`,
                                    id_ent
                                )
                                return Promise.resolve(response)
                            } else {
                                throw 'Не статус 201 ';
                            }
                        })
                        .catch(error => {
                            console.error('Ошибка: ', error);
                        });
                }
                break;

            case `${ActionTypes.STAFF}`:
                {
                    const { id_dept_staff } = item;
                    const options = {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'POST',
                        body: JSON.stringify(item),
                    };

                    fetch(`${UrlTypes.URL_STAFF}`, options)
                        .then(response => {
                            if (response.status === 201) {
                                this.showComponent(
                                    `${UrlTypes.URL_STAFF}`,
                                    `${ActionTypes.STAFF}`,
                                    id_dept_staff
                                )
                                return Promise.resolve(response)
                            } else {
                                throw 'Не статус 201 ';
                            }
                        })
                        .catch(error => {
                            console.error('Ошибка: ', error);
                        });
                }
                break;
        }

    }

    editList = (listType: string, item: any) => {
        switch (listType) {
            case `${ActionTypes.ORGANIZATIONS}`:
                {
                    const { id } = item;
                    // this.dispatch({ type: `${AsyncActionTypes.BEGIN}` });
                    const options = {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'PUT',
                        body: JSON.stringify(item),
                    };

                    fetch(`${UrlTypes.URL_ORGANIZATION}/${id}`, options)
                        .then(response => {
                            if (response.status === 200) {
                                //this.dispatch({ type: `${ActionTypes.EDIT}${ActionTypes.ORGANIZATIONS}${AsyncActionTypes.SUCCESS}`});
                                this.showComponent(
                                    `${UrlTypes.URL_ORGANIZATION}`,
                                    `${ActionTypes.ORGANIZATIONS}`
                                )
                                return Promise.resolve(response)
                            } else {
                                throw 'Не статус 200 ';
                            }
                        })
                        .catch(error => {
                            console.error('Ошибка: ', error);
                        });
                }
                break;

            case `${ActionTypes.DEPTS}`:
                {
                    const { id, id_ent } = item;
                    // this.dispatch({ type: `${AsyncActionTypes.BEGIN}` });
                    const options = {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'PUT',
                        body: JSON.stringify(item),
                    };

                    fetch(`${UrlTypes.URL_DEPT}/${id}`, options)
                        .then(response => {
                            if (response.status === 200) {
                                //this.dispatch({ type: `${ActionTypes.EDIT}${ActionTypes.ORGANIZATIONS}${AsyncActionTypes.SUCCESS}`});
                                this.showComponent(
                                    `${UrlTypes.URL_DEPT}`,
                                    `${ActionTypes.DEPTS}`,
                                    id_ent
                                )
                                return Promise.resolve(response)
                            } else {
                                throw 'Не статус 200 ';
                            }
                        })
                        .catch(error => {
                            console.error('Ошибка: ', error);
                        });
                }
                break;

            case `${ActionTypes.STAFF}`:
                {
                    const { id, id_dept_staff } = item;
                    const options = {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'PUT',
                        body: JSON.stringify(item),
                    };

                    fetch(`${UrlTypes.URL_STAFF}/${id}`, options)
                        .then(response => {
                            if (response.status === 200) {
                                this.showComponent(
                                    `${UrlTypes.URL_STAFF}`,
                                    `${ActionTypes.STAFF}`,
                                    id_dept_staff
                                )
                                return Promise.resolve(response)
                            } else {
                                throw 'Не статус 200 ';
                            }
                        })
                        .catch(error => {
                            console.error('Ошибка: ', error);
                        });
                }
                break;
        }

        // this.dispatch({ type: `EDIT_${listType}`,payload: id });
    }
    // Вывод списка
    showComponent = (url: string, listType: string, id?: number) => {
        this.dispatch({ type: `${ActionTypes.SHOW}${listType}${AsyncActionTypes.BEGIN}` });
        const options = {
            method: 'GET',
            //body: JSON.stringify(loginData),
        };
        fetch(url, options)
            .then(response => {
                if (response.status === 200) {
                    return Promise.resolve(response)
                } else {
                    throw 'Не статус 200 ';
                }
            })
            .then(response => {
                return response.json()
            })
            .then(data => {
                let newData = data;
                //Т.к. в json-server используется id,а у меня все приложение id по разному называется, потому id перевожу в соответствующее название моего приложения

                switch (listType) {
                    case `${ActionTypes.ORGANIZATIONS}`:
                        newData = data.map(function (item: IOrganization) {
                            return item;
                        })
                        break;
                    case `${ActionTypes.DEPTS}`:
                        newData = data.map(function (item: IDept) {
                            return item;
                        })
                        break;
                    case `${ActionTypes.STAFF}`:
                        newData = data.map(function (item: IStaff) {
                            return item;
                        })
                        break;
                }

                if (id) {
                    this.dispatch({ type: `GET_ID_PARENT`, payload: id });
                    newData = newData.filter((item: any) => {
                        if ((item.id_ent === id) || ('' + item.id_ent === '' + id)) {
                            return item;
                        }
                        if ((item.id_dept_staff === id) || ('' + item.id_dept_staff === '' + id)) {
                            return item;
                        }
                    })
                }
                this.dispatch({ type: `${ActionTypes.SHOW}${listType}${AsyncActionTypes.SUCCESS}`, payload: newData });
            })
            .catch(error => {
                this.dispatch({ type: `${ActionTypes.SHOW}${listType}${AsyncActionTypes.FAILURE}`, payload: error });
            });
    };

    //валидация формы
    validation = (loginData: ILoginValidation) => {
        this.dispatch({ type: `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}` });
        const options = {
            method: 'GET',
            //body: JSON.stringify(loginData),
        };
        fetch(UrlTypes.URL_LOGIN, options)
            .then(response => {
                if (response.status === 200) {
                    return Promise.resolve(response)
                } else {
                    throw 'Не статус 200 ';
                }
            })
            .then(response => {
                return response.json()
            })
            .then(data => {
                if (loginData.login === data.login && loginData.pass === data.password) {
                    this.dispatch({ type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}` });
                } else {
                    throw 'Данные не верны';
                }
            })
            .catch(error => {
                this.dispatch({ type: `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`, payload: error });
            });
    }

    onClick = async (increment: number) => {
        //Простейший асинхронный экшен
        // setTimeout(() => {
        //   this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`});
        // }, 2000);

        this.dispatch({ type: `${ActionTypes.CLICK}${AsyncActionTypes.BEGIN}` });
        await new Promise((resolve) => setTimeout(resolve, 2000));
        this.dispatch({ type: `${ActionTypes.CLICK}${AsyncActionTypes.SUCCESS}`, payload: increment });
    };

    onLogin = (loginData: ILoginData) => {
        this.dispatch({ type: `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}` });

        const options = {
            method: 'POST',
            body: JSON.stringify(loginData),
        };
        fetch('http://127.0.0.1:8080/login', options)
            .then(response => {
                if (response.status === 200) {
                    this.dispatch({ type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}` });
                } else {
                    throw 'error';
                }
            })
            .catch(error => {
                this.dispatch({ type: `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`, payload: error });
            });
    };

    onLogout = () => {
        const options = {
            method: 'POST',
        };
        fetch('http://127.0.0.1:8080/logout', options)
            .then(response => {
                if (response.status === 200) {
                    this.dispatch({ type: ActionTypes.LOGOUT });
                } else {
                    throw 'error';
                }
            })
            .catch(() => {
                this.dispatch({ type: ActionTypes.LOGOUT });
            });
    }
}

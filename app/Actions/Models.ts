/**
 * Модель данных для авторизации.
 * @prop {string} name Имя пользователя.
 * @prop {string} password Пароль пользователя.
 */
export interface ILoginData {
    name: string;
    password: string;
}

export interface IOrganization {
    id: number;
    name_entity: string;
    addr_entity: string;
    inn_entity: string;
  }
  
  export interface IDept {
    id: number;
    id_ent: number;
    name_dept: string;
    tel_dept: string;
  }
  
  export interface IStaff {
    id: number;
    id_dept_staff: number;
    fio_staff: string;
    addr_staff: string;
    job_staff: string;
  }
  
  
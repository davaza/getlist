/**
 * Типы экшенов, используемые в приложении.
 * LOGIN - Авторизация.
 * LOGOUT - Отмена авторизации.
 * CLICK - Подсчёт чего-либо для примера.
 * SHOW - Показ чего-либо
 * ORGANIZATIONS - Выбор списка организации
 * DEPTS - Выбор списка отделов
 * STAFF - Выбор списка сотрудников
 */
export enum ActionTypes {
    LOGIN = 'ACTION_LOGIN',
    LOGOUT = 'ACTION_LOGOUT',
    CLICK = 'ACTION_CLICK',
    SHOW = 'ACTION_SHOW',
    ORGANIZATIONS = 'ACTION_ORGANIZATIONS',
    DEPTS = 'ACTION_DEPTS',
    STAFF = 'ACTION_STAFF',
    EDIT = 'ACTION_EDIT',
    DELETE = 'ACTION_DELETE',
}

// export enum UrlTypes {
//     URL_ORGANIZATION = 'http://www.mocky.io/v2/5c0618a53300005600e8142c',
//     URL_DEPT = 'http://www.mocky.io/v2/5c1cb4413100004c0010417b',
//     URL_STAFF = 'http://www.mocky.io/v2/5c1cb4e13100005200104185'
// }

export enum UrlTypes {
    URL_ORGANIZATION = 'http://localhost:3000/entity',
    URL_DEPT = 'http://localhost:3000/depts',
    URL_STAFF = 'http://localhost:3000/staff',
    URL_LOGIN = 'http://localhost:3000/login'
}

// export enum UrlTypes {
//     URL_ORGANIZATION = 'http://localhost/boilerplate_data/entity.php',
//     URL_DEPT = 'http://localhost/boilerplate_data/dept.php',
//     URL_STAFF = 'http://localhost/boilerplate_data/staff.php'
// }

/**
 * Подтипы для экшенов при ассинхронной работы.
 * BEGIN - Начало ассинхронного действия.
 * SUCCESS - Действие завершилось успешно.
 * FAILURE - Действие завершилось с ошибкой.
 */
export enum AsyncActionTypes {
    BEGIN = '_BEGIN',
    SUCCESS = '_SUCCESS',
    FAILURE = '_FAILURE',
}
